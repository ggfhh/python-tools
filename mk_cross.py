#!/usr/bin/python3
################################################################################
# Tool create gcc-cross compiler for different targets on debian system
################################################################################
import argparse
import datetime
import getpass
import logging
import multiprocessing
import os
import pathlib
import re
import subprocess
import traceback

log = logging.getLogger(__name__)

_version = "0.02"

_targets = ['m68k-elf', 'powerpc-eabi', 'arm-eabi']

# ARM list list details see: gcc/config/arm/t-multilib
_options = {
    'm68k-elf': '',
    'powerpc-eabi': '',
    'arm-eabi': '--enable-interwork --enable-multilib --with-multilib-list=armv6s-m,armv7-m,armv7e-m,armv8-m.base,armv8-m.main,armv7,vfpv3-d16,fpv4-sp-d16,fpv5-sp-d16,fpv5-d16'
}
_build_path = 'build'
_cpu_cnt = None

_opt_nlib = {
    'enable-newlib-io-pos-args': 'no',
    'enable-newlib-io-c99-formats': 'no',
    # 'enable-newlib-register-fini':            'no',
    'enable-newlib-io-long-long': 'no',
    'enable-newlib-io-long-double': 'no',
    # 'enable-newlib-mb':                       'no',
    # 'enable-newlib-iconv-encodings':          'no',
    # 'enable-newlib-iconv-from-encodings':     'no',
    # 'enable-newlib-iconv-to-encodings':       'no',
    # 'enable-newlib-iconv-external-ccs':       'no',
    'enable-newlib-atexit-dynamic-alloc': 'no',
    'enable-newlib-global-atexit': 'yes',
    'enable-newlib-global-stdio-streams': 'yes',
    'enable-newlib-reent-small': 'yes',
    # 'enable-newlib-fvwrite-in-streamio':      'yes',
    'enable-newlib-fseek-optimization': 'no',
    # 'enable-newlib-wide-orient':              'no',
    'enable-newlib-nano-malloc': 'yes',
    'enable-newlib-unbuf-stream-opt': 'no',
    'enable-newlib-long-time_t': 'yes',
    # 'enable-multilib':                        'yes',
    'enable-target-optspace': 'yes',
    'enable-newlib-multithread': 'no',
    # 'enable-newlib-iconv':                    'no',
    # 'enable-newlib-elix-level':               '0',
    'enable-newlib-io-float': 'no',
    'enable-newlib-supplied-syscalls': 'no',
    'enable-lite-exit': 'yes',
    'enable-newlib-nano-formatted-io': 'yes'
}


def _adjust_newlib(target, install_name, sup_float, sup_llong, sup_thread):
    if sup_float:
        _opt_nlib['enable-newlib-io-float'] = 'yes'
        if sup_llong:
            _opt_nlib['enable-newlib-io-long-double'] = 'yes'
        else:
            _opt_nlib['enable-newlib-io-long-double'] = 'no'
    else:
        _opt_nlib['enable-newlib-io-float'] = 'no'

    if sup_llong:
        _opt_nlib['enable-newlib-io-long-long'] = 'yes'
    else:
        _opt_nlib['enable-newlib-io-long-long'] = 'no'
    if sup_thread:
        _opt_nlib['enable-newlib-multithread'] = 'yes'
        _opt_nlib['enable-newlib-global-atexit'] = 'no'
        _opt_nlib['enable-newlib-global-atexit'] = 'no'
        _opt_nlib['enable-newlib-global-stdio-streams'] = 'no'
    else:
        _opt_nlib['enable-newlib-multithread'] = 'no'
        _opt_nlib['enable-newlib-global-atexit'] = 'yes'
        _opt_nlib['enable-newlib-global-atexit'] = 'yes'
        _opt_nlib['enable-newlib-global-stdio-streams'] = 'yes'
    install_path = os.getcwd()
    res = f" --prefix={install_path}/{_build_path}/install/{target}_{install_name}"
    for k in _opt_nlib.keys():
        res += f' --{k}={_opt_nlib[k]}'
    return res


def _log_to_file(file_name, stderr, stdout):
    out = stdout.decode('utf-8') if stdout else ""
    if file_name:
        err = stderr.decode('utf-8') if stderr else ""
        f = open(file_name, 'w')
        f.write("################################################################################\n")
        f.write("# STDERROR:\n")
        f.write("#===============================================================================\n")
        f.write(err)
        f.write("################################################################################\n")
        f.write("\n")
        f.write("################################################################################\n")
        f.write("# STDOUT:\n")
        f.write("#===============================================================================\n")
        f.write(out)
        f.write("################################################################################\n")
        f.close()
    return out


def _call_program(data, log_file=None):
    log.debug(f"Execute command: {data}")
    cmd = data.split(' ')
    res = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out = _log_to_file(log_file, res.stderr, res.stdout)
    lines = out.split('\n')
    return lines


def _call_sudo(pw, param, log_file):
    if pw:
        cmd_str = 'sudo -S ' + param
        log.debug(f"Execute sudo-command: {cmd_str}")
        cmd = cmd_str.split(' ')
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
        p.stdin.write((pw + "\n").encode())
        p.stdin.write("y\n".encode())
        p.stdin.flush()
        out, err = p.communicate()
        res = _log_to_file(log_file, err, out).split('\n')
        return res
    log.debug(f"Sudo execute: {param}")
    cmd_str = 'sudo ' + param
    return _call_program(cmd_str, log_file)


def _check_tooling(pw, do_install):
    tools = ['libgmp3-dev', 'libgmp-dev', 'libmpfr-dev', 'libmpc-dev', 'yasm', 'libfuse-dev', 'bison', 'python2-dev',
             'python3-dev', 'build-essential', 'gawk', 'libfl-dev']
    log.debug(f"Check for installed packages: {tools}")
    to_install = ""
    out = _call_program('dpkg-query -l')
    for tool in tools:
        add_tool = tool
        for line in out:
            if re.search(f"^ii\\s+{tool}[ :]", line):
                add_tool = None
                break
        if add_tool:
            to_install += f" {add_tool}"
    if to_install:
        if do_install:
            log.info(f"Need to install: {to_install}")
            out = _call_sudo(pw, f"apt-get install --yes {to_install}", 'apt_install.log')
            log.debug(f"Install output: {out}")
        else:
            log.warning(f"Need to install: {to_install}")
    else:
        log.debug("Nothing to install")


def _get_file_name(target, tool, op=None, ext=None):
    if op:
        if ext:
            return f"{_build_path}/{target}/{tool}_{op}{ext}"
        return f"{_build_path}/{target}/{tool}_{op}"
    return f"{_build_path}/{target}/{tool}"


def _write_step_file(file):
    f = open(file, "w")
    time_string = str(datetime.datetime.now())
    f.write(time_string)
    f.close()
    log.debug(f"Write timestamp: {time_string}\n")


def _create_cross_step(target, tool, tag, cmd, op, pw=None):
    log.debug(f"create cross-tool: {tool} for {target} using tag={tag} cmd={cmd} op={op}")
    step_file = _get_file_name(target, tag, op)
    build_path = _get_file_name(target, tool)
    pathlib.Path(build_path).mkdir(parents=True, exist_ok=True)
    log.debug(f"Building with folder: '{build_path}'")
    if not os.path.isfile(step_file):
        os.chdir(build_path)
        if pw:
            _call_sudo(pw, cmd, f"{tag}_{op}.log")
        else:
            _call_program(cmd, f"{tag}_{op}.log")
        os.chdir('../../..')
        _write_step_file(step_file)
    else:
        log.debug(f"Skippp: {target} {tool} since already build!")


def _create_cross(pw, target, tool, cmd_config, cmd_make, cmd_install, tag=None):
    if not tag:
        tag = tool
    if cmd_config:
        _create_cross_step(target, tool, tag, cmd_config, 'config')
    if cmd_make:
        cpu_count = multiprocessing.cpu_count()
        _create_cross_step(target, tool, tag, cmd_make + f" -j {cpu_count}", 'make')
    if cmd_install:
        _create_cross_step(target, tool, tag, cmd_install, 'install', pw)


def main():
    """Parse arguments and call build for target"""
    p = argparse.ArgumentParser()
    p.add_argument('--log-level', default=logging.INFO, type=lambda x: getattr(logging, x), help='Config logging')
    p.add_argument('-d', '--disable_gcc', action='store_true', required=False, default=False,
                   help="Don't build binutils and gcc")
    p.add_argument('-m', '--micro_newlib', action='store_true', required=False, default=False,
                   help='Create additional smaller newlib (no floast, not thread)')
    p.add_argument('-M', '--micro_threaddnewlib', action='store_true', required=False, default=False,
                   help='Create additional smaller newlib (no floast)')
    p.add_argument('-n', '--nano_newlib', action='store_true', required=False, default=False,
                   help='Create additional tiny newlib (no floast, thread, long long)')
    p.add_argument('-N', '--nano_threadnewlib', action='store_true', required=False, default=False,
                   help='Create additional tiny newlib (no floast, long long)')
    p.add_argument('-s', '--store_password', action='store_true', required=False, default=False,
                   help='Ask once for sudo password at beginning')
    p.add_argument('-i', '--install_packages', action='store_true', required=False, default=False,
                   help='Automatically install required packages')
    p.add_argument('-v', '--version', action='store_true', required=False, default=False,
                   help='Show version information')
    p.add_argument('targets', nargs='+', help=f"set target {_targets}")
    a = p.parse_args()
    logging.basicConfig(level=a.log_level)
    if a.version:
        print(f"mk_cross.py Version: {_version}")
    try:
        logging.debug(f"Arguments: {a}")
        for target in a.targets:
            if target not in _targets:
                raise Exception(f"{target} not in targets: {_targets}")
        pw = getpass.getpass('Password:') if a.store_password else None
        _check_tooling(pw, a.install_packages)
        for target in a.targets:
            logging.info(f"Processing: {target}")
            if target in _targets:
                newlib_cfg = f"../../../newlib-cygwin/configure --target={target}"
                if not a.disable_gcc:
                    _create_cross(pw, target, 'binutils-gdb', f"../../../binutils-gdb/configure --target={target}",
                                  'make all', 'make install')
                    _create_cross(pw, target, 'gcc',
                                  f"../../../gcc/configure --target={target} {_options[target]} --without-headers --with-newlib --with-gnu-as --with-gnu-ld --enable-languages=c,c++ --with-system-zlib",
                                  "make all-gcc", "make install-gcc")
                    _create_cross(pw, target, 'newlib', newlib_cfg, 'make all', 'make install')
                    _create_cross(pw, target, 'gcc', None, 'make all', 'make install', 'gcc-full')
                    # _create_cross(pw, target, 'gdb', f"../../../gdb/configure --target={target}", 'make all', 'make install')
                if a.micro_newlib:
                    lib_name = 'newlib-m'
                    cfg = _adjust_newlib(target, lib_name, False, True, False)
                    _create_cross(pw, target, lib_name, newlib_cfg + cfg, 'make all', 'make install')
                if a.micro_threaddnewlib:
                    lib_name = 'newlib-mt'
                    cfg = _adjust_newlib(target, lib_name, False, True, True)
                    _create_cross(pw, target, lib_name, newlib_cfg + cfg, 'make all', 'make install')
                if a.nano_newlib:
                    lib_name = 'newlib-n'
                    cfg = _adjust_newlib(target, lib_name, False, False, False)
                    _create_cross(pw, target, lib_name, newlib_cfg + cfg, 'make all', 'make install')
                if a.nano_threadnewlib:
                    lib_name = 'newlib-nt'
                    cfg = _adjust_newlib(target, lib_name, False, False, True)
                    _create_cross(pw, target, lib_name, newlib_cfg + cfg, 'make all', 'make install')
    except Exception as e:
        log.error('Failed to build cross-tooling: ' + str(e))
        if log.isEnabledFor(logging.DEBUG):
            log.error('Callstack: ' + traceback.format_exc())
        else:
            log.error('Callstack: ' + traceback.format_stack())
        exit(-1)


if __name__ == "__main__":
    main()
