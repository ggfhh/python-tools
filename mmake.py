#!/usr/bin/python
# Application to generate a makefile for gnu compiler (by using gcc)
import argparse
import glob
import logging
import os
import traceback

log = logging.getLogger(__name__)

start_line = '######## START MMAKE-INSERT ##################################################'
end_line = '######## END MMAKE-INSERT ####################################################'
version = '0.00'

class SourceFiles:
    sources_c = []
    sources_cpp = []
    sources_s = []

    def _add_file(self, source_file):
        if source_file.endswith('.c'):
            log.debug(f"\tAdd C-File: {source_file}")
            self.sources_c.append(file)
        elif source_file.endswith('.cpp') or source_file.endswith('.c++') or source_file.endswith('.cxx'):
            log.debug(f"\tAdd C++-File: {source_file}")
            self.sources_c.append(source_file)
        elif source_file.endswith('.s') or source_file.endswith('.asm'):
            log.debug(f"\tAdd assembly-File: {source_file}")
            self.sources_s.append(source_file)
        else:
            log.debug(f"Skipp file {source_file} since wrong type!")

    def _add_dir(self, folder, recursive):
        logging.debug(f"Adding folder {folder} (recursive={recursive})")
        for file_entry in glob.glob(folder):
            if os.path.isfile(file_entry):
                self._add_file(file_entry)
            elif os.path.isdir(file_entry):
                if recursive:
                    self._add_dir(file_entry, recursive)
                else:
                    log.debug(f"Skipp folder {file_entry}")
            else:
                log.debug(f"Skipp no file/folder: {file_entry}")

    def add_content(self, path, recursive):
        for file in path:
            if os.path.isfile(file):
                self._add_file(file)
            elif os.path.isdir(file):
                self._add_dir(file, recursive)
            else:
                log.error(f"Given parameter '{path}' is not a file or directory!")

    def get_c_files(self):
        return self.sources_c

    def get_cpp_files(self):
        return self.sources_cpp

    def get_asm_files(self):
        return self.sources_s

def _get_header(file_name):
    folder_name = 'TODO: get parent name of Makefile :-)'
    if os.path.isfile(file_name):
        data = []
        file_handle = open(file_name, 'r')
        for line in file_handle:
            if line ==start_line:
                break
            data.append(line)
        file_handle.close()
        return data
    return [
        '#***************************************************************************'
        f"# Created makefile in folder {folder_name}"
        "#==========================================================================="
        f"# created by make makefile version {version}"
        '#***************************************************************************'
        ''
        f"PROJECT\t\t= {folder_name}"
        'EXT\t\t='
        'PREFIX\t\t='
        ''


    ]
	'#---------------------------- MACRO DEFINITIONS -------------------------------',
	"PROJECT     = $prj_name",
	'EXT         = ',
	'PREFIX      = ',
	'PRJPATH     = ./',
	"RELPATH     = $bpath_rel/",
	"DBGPATH     = $bpath_dbg/",
	'LIBPATH     = ',
	'',
	'#################################################',
	'# os-commands',
	'#################################################',
	'RM_CMD      = rm -r',
	'CP_CMD      = cp',
	'MV_cMD      = mv',
	'LN_CMD      = ln -s',
	'MKDIR_CMD   = mkdir -p',
	'#################################################',
	'# makefile-options',
	'#################################################',
	'BUILD_ARCH	= ',
	'BUILD_TYPE	= ',
	'BUILD_OPT	= ',
	'BUILD_O_C	= -std=gnu18',
	'BUILD_O_CPP	= -std=gnu++17',
	'BUILD_O_C_CPP	= ',
	'BUILD_O_ASM	= ',
	'BUILD_O_LD	= ',
	'BUILD_LD_SOUT	= -o',
	'BUILD		= ',
	'BUILD_R	= -O4',
	'BUILD_D	= -g -Og',
	'BUILD_DEP	= ',
	'BUILD_DEP_R	= ',
	'BUILD_DEP_D	= ',
	'BUILD_LNK	= ',
	'BUILD_LNK_R	= ',
	'BUILD_LNK_D	= ',
	'BUILD_LNKDEP	= ',
	'BUILD_LNKDEP_R	= ',
	'BUILD_LNKDEP_D	= ',
	'BUILD_LIBS	= ',
	'BUILD_LIBS_R	= ',
	'BUILD_LIBS_D	= ',
	'BUILD_VER	= ',
	'WARNINGS	= -Wall -Wextra -Wpedantic',
	'DEFINES	= ',
	'DEFINES_R	= ',
	'DEFINES_D	= -DDEBUG',
	"ARCHIV		= $makefilename ",
	'',
	'#################################################',
	'# the following lines are for gnu-compiler',
	'#################################################',
	'',
	'# --------',
	'# Compiler',
	'# --------',
	'CC      = $(BUILD_ARCH)gcc',
	'CC_OPT  = $(BUILD_TYPE) $(BUILD_OPT) $(BUILD_O_C_CPP) $(BUILD_O_C) $(WARNINGS) $(BUILD) $(BUILD_) $(DEFINES_) -c',
	'',
	'# --------',
	'# CPP-Compiler',
	'# --------',
	'CPP	 = $(BUILD_ARCH)g++',
	'CPP_OPT = $(BUILD_TYPE) $(BUILD_OPT) $(BUILD_O_C_CPP) $(BUILD_O_CPP) $(WARNINGS) $(BUILD) $(BUILD_) $(DEFINES_) -c',
	'',
	'# --------',
	'# ASM-Compiler',
	'# --------',
	"ASM	 = $def_as_tool",
	'ASM_OPT = $(BUILD_TYPE) $(BUILD_OPT) $(BUILD_O_ASM) $(WARNINGS) $(BUILD) $(BUILD_) $(DEFINES_) -c',
	'',
	'# --------',
	'# Linker',
	'# --------',
	"LD      = $def_ld_tool",
	'LD_OPT  = $(BUILD_TYPE) $(BUILD_OPT) $(BUILD_LIBS) $(BUILD_LIBS_) $(BUILD_O_LD) $(BUILD_LNK) $(BUILD_LNK_) $(BUILD) $(BUILD_)',
	'',
	'# --------',
	'# Doc-generation',
	'# --------',
	"DOC     = $doc_cmd",
	"DOC_OPT = $doc_cfg",
	'',
	'# --------',
	'# zip-generation',
	'# --------',
	'ZIP     = xz',
	'ZIP_EXT = .xz',
	'',
	'# --------',
	'# objcopy',
	'# --------',
	'OC      = $(BUILD_ARCH)objcopy',
	'OC_OPT  = ',
	'',
	'# --------',
	'# objdump',
	'# --------',
	'OD      = $(BUILD_ARCH)objdump',
	'OD_OPT  = ',
	'',
	'#################################################',
	'# now build debug/release version',
	'#################################################',
	'debug:',
	"\tmake \$(DBGPATH)\$(PREFIX)\$(PROJECT)\$(EXT) BUILD_PATH=\$(DBGPATH) BUILD_='\$(BUILD_D)' BUILD_DEP_='\$(BUILD_DEP_D)' BUILD_LNK_='\$(BUILD_LNK_D)' BUILD_LINKDEP_='\$(BUILD_LINKDEP_D)' BUILD_LIBS_='\$(BUILD_LIBS_D)' DEFINES_='\$(DEFINES_D)'",
	'',
	'release:',
	"\tmake \$(RELPATH)\$(PREFIX)\$(PROJECT)\$(EXT) BUILD_PATH=\$(RELPATH) BUILD_='\$(BUILD_R)' BUILD_DEP_='\$(BUILD_DEP_R)' BUILD_LNK_='\$(BUILD_LNK_R)' BUILD_LINKDEP_='\$(BUILD_LINKDEP_R)' BUILD_LIBS_='\$(BUILD_LIBS_R)' DEFINES_='\$(DEFINES_R)'",
	'',
	'all:',
	"\tmake \$(DBGPATH)\$(PREFIX)\$(PROJECT)\$(EXT) BUILD_PATH=\$(DBGPATH) BUILD_='\$(BUILD_D)' BUILD_DEP_='\$(BUILD_DEP_D)' BUILD_LNK_='\$(BUILD_LNK_D)' BUILD_LINKDEP_='\$(BUILD_LINKDEP_D)' BUILD_LIBS_='\$(BUILD_LIBS_D)' DEFINES_='\$(DEFINES_D)'",
	"\tmake \$(RELPATH)\$(PREFIX)\$(PROJECT)\$(EXT) BUILD_PATH=\$(RELPATH) BUILD_='\$(BUILD_R)' BUILD_DEP_='\$(BUILD_DEP_R)' BUILD_LNK_='\$(BUILD_LNK_R)' BUILD_LINKDEP_='\$(BUILD_LINKDEP_R)' BUILD_LIBS_='\$(BUILD_LIBS_R)' DEFINES_='\$(DEFINES_R)'",
	'',
	);
}


for line in file1:
    count += 1
    print("Line{}: {}".format(count, line.strip()))






################## commments


# Python program to
# demonstrate readline()

L = ["Geeks\n", "for\n", "Geeks\n"]

# Writing to a file
file1 = open('myfile.txt', 'w')
file1.writelines((L))
file1.close()

# Using readline()
file1 = open('myfile.txt', 'r')
count = 0

while True:
    count += 1

    # Get next line from file
    line = file1.readline()

    # if line is empty
    # end of file is reached
    if not line:
        break
    print("Line{}: {}".format(count, line.strip()))

file1.close()

#################### comments















def _output_header(file, target_name):




# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    p = argparse.ArgumentParser()
    p.add_argument('--log-level', default=logging.DEBUG, type=lambda x: getattr(logging, x), help='Configure the logging level.')
    p.add_argument('-r', '--recursive', default=False, help='Search folders recursive')
    p.add_argument('-v', '--version', default=False, help='Show version information')
    p.add_argument('files', nargs='+')
    a = p.parse_args()
    logging.basicConfig(level=a.log_level)
    try:
        sources = SourceFiles()
        logging.debug(f"Calling make make using gcc with parameters: {a}")
        sources.add_content(a.files, a.recursive)
        content = _get_header("Makefile")



        logging.info("Ready!")
    except Exception as e:
        log.error("Failed to create Makefile because of error: " + str(e))
        if log.isEnabledFor(logging.DEBUG):
            log.error("Callstack:\n" + traceback.format_stack())
        else:
            log.error("Callstack:\n" + traceback.format_stack())
            exit(-1)
